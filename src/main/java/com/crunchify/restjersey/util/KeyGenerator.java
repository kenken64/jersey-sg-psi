package com.crunchify.restjersey.util;

import java.security.Key;

public interface KeyGenerator {

    Key generateKey();
}