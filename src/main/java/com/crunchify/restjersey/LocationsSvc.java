package com.crunchify.restjersey;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.crunchify.restjersey.jpa.core.Locations;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.crunchify.restjersey.jwt.JWTTokenNeeded;
@Path("locations")
public class LocationsSvc {

    @GET
    @Produces("application/json")
    @JWTTokenNeeded
    public String getLocations() {
        String jsonData = PSIUtil.getDataGovPSIData();
        Gson gson = new GsonBuilder().create();
        Locations p = gson.fromJson(jsonData, Locations.class);
        return jsonData;
    }

}