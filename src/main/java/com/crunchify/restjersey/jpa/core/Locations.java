package com.crunchify.restjersey.jpa.core;

import java.math.BigDecimal;

import javax.persistence.Entity;

@Entity
public class Locations extends AbstractEntity {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getLattitude() {
        return lattitude;
    }

    public void setLattitude(BigDecimal lattitude) {
        this.lattitude = lattitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    private String name;
    private BigDecimal lattitude, longitude;

    public Locations(String name, BigDecimal lattitude, BigDecimal longitude ) {
        this.name = name;
        this.lattitude = lattitude;
        this.longitude = longitude;
    }

    protected Locations() {

    }

}