package com.crunchify.restjersey.jpa.core;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


@Transactional
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Returns the {@link User} with the given identifier.
     *
     * @param id the id to search for.
     * @return
     */
    User findOne(Long id);

    /**
     * Saves the given {@link User}.
     *
     * @param user the {@link User} to search for.
     * @return
     */
    User save(User user);
    
    /**
     * Returns the User with the given {@link EmailAddress}.
     *
     * @param emailAddress the {@link EmailAddress} to search for.
     * @return
     */
    User findByEmailAddress(EmailAddress emailAddress);
}