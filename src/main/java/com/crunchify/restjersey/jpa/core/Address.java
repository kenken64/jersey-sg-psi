package com.crunchify.restjersey.jpa.core;

import javax.persistence.Entity;

import org.springframework.util.Assert;


@Entity
public class Address extends AbstractEntity {

    private String address1, address2, street, city, country;

    /**
     * Creates a new {@link Address} from the given street, city and country.
     *
     * @param street must not be {@literal null} or empty.
     * @param city must not be {@literal null} or empty.
     * @param country must not be {@literal null} or empty.
     * @param address1 must not be {@literal null} or empty.
     * @param address2 must not be {@literal null} or empty.
     */
    public Address(String street, String city, String country, String address1, String address2) {

        Assert.hasText(street, "Street must not be null or empty!");
        Assert.hasText(city, "City must not be null or empty!");
        Assert.hasText(country, "Country must not be null or empty!");
        Assert.hasText(address1, "Address1 must not be null or empty!");
        Assert.hasText(address2, "Address2 must not be null or empty");
        this.street = street;
        this.city = city;
        this.country = country;
        this.address1 = address1;
        this.address2 = address2;
    }

    protected Address() {

    }

    /**
     * Returns a copy of the current {@link Address} instance which is a new entity in terms of persistence.
     *
     * @return
     */
    public Address getCopy() {
        return new Address(this.street, this.city, this.country, this.address1, this.address2);
    }

    /**
     * Returns the street.
     *
     * @return
     */
    public String getStreet() {
        return street;
    }

    /**
     * Returns the address1.
     *
     * @return
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Returns the address2
     *
     * @return
     */
    public String getAddress2(){
        return address2;
    }

    /**
     * Returns the city.
     *
     * @return
     */
    public String getCity() {
        return city;
    }

    /**
     * Returns the country.
     *
     * @return
     */
    public String getCountry() {
        return country;
    }
}