package com.crunchify.restjersey;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.crunchify.restjersey.jwt.JWTTokenNeeded;

@Path("psi")
public class PSI {

    @GET
    @Produces("application/json")
    @JWTTokenNeeded
    public String getPSI(@QueryParam("location") String message) {
        String jsonData = PSIUtil.getDataGovPSIData();
        return jsonData;
    }

}
