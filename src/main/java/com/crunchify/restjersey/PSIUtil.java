package com.crunchify.restjersey;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class PSIUtil {

    public static String getDataGovPSIData(){
        Client client = ClientBuilder.newClient();

        WebTarget resource = client.target("https://api.data.gov.sg/v1/environment/psi");
        Invocation.Builder request = resource.request();
        request.accept(MediaType.APPLICATION_JSON);
        request.header("api-key", "pPxC4fKrC0mCRtm75vi5F82RpnZPKGRm");
        Response response = request.get();

        System.out.println(response.getStatus());
        String jsonData = response.readEntity(String.class);
        return jsonData;
    }
}
