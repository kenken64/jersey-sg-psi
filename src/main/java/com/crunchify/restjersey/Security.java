package com.crunchify.restjersey;

import com.crunchify.restjersey.jpa.core.EmailAddress;
import com.crunchify.restjersey.jpa.core.User;
import com.crunchify.restjersey.jpa.core.UserRepository;
import com.crunchify.restjersey.jpa.core.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import static org.glassfish.grizzly.http.util.Header.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import java.security.Key;
import javax.inject.Inject;

import com.crunchify.restjersey.jwt.JWTTokenNeeded;
import com.crunchify.restjersey.util.SimpleKeyGenerator;

@Path("/security")
public class Security {
    @Autowired
    UserRepository repository;

    //@Autowired
    //SimpleKeyGenerator keyGenerator;
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Path("register")
    @POST
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response registerUser(
            @FormParam("username") String username,
            @FormParam("password") String password,
            @FormParam("firstName") String firstName,
            @FormParam("lastName") String lastName,
            @FormParam("street") String street,
            @FormParam("city") String city,
            @FormParam("country") String country,
            @FormParam("address1") String address1,
            @FormParam("address2") String address2

            ) {

        String hashedPassword = passwordEncoder().encode(password);
        EmailAddress emailaddy = new EmailAddress(username);
        User u = repository.findByEmailAddress(emailaddy);
        if(u != null){
            return Response.status(403).build();
        }
        User uu = new User(firstName, lastName);
        System.out.println(emailaddy.toString());
        uu.setEmailAddress(emailaddy);
        System.out.println(hashedPassword);
        uu.setPassword(hashedPassword);
        System.out.println(hashedPassword);
        uu.add(new Address(street, city, country, address1, address2));
        System.out.println("xxx");
        User uus = repository.save(uu);
        System.out.println(uus.getId());
        //System.out.println(uus.getSQL());
        Gson gson = new GsonBuilder().create();
        String jsonInString = gson.toJson(uus);
        System.out.println(jsonInString);
        //return Response.ok("{ \"status\": \"done\" }").build();
        return Response.ok("{ \"status\": \"done\" }").build();
    }

    @Path("login")
    @POST
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response loginUser(
            @FormParam("username") String username,
            @FormParam("password") String password
    ) {
        System.out.println(" Username : " + username);
        System.out.println(" Password : " + password);
        EmailAddress emailaddy = new EmailAddress(username);
        User u = repository.findByEmailAddress(emailaddy);
        System.out.println(" u : " + u);
        if(u == null){
            return Response.status(403).build();
        }
        String storedPassword = u.getPassword();
        System.out.println(" storedPassword : " + storedPassword);
        CharSequence loginPassword = password;
        System.out.println(" loginPassword : " + loginPassword);
        System.out.println(" ok? : " + passwordEncoder().matches(loginPassword,storedPassword));
        if(!passwordEncoder().matches(loginPassword,storedPassword)){
            return Response.status(403).build();
        }
        SimpleKeyGenerator genkey = new SimpleKeyGenerator();
        Key key = genkey.generateKey();
        String compactJws = Jwts.builder()
            .setSubject(password)
            .signWith(SignatureAlgorithm.HS512, key)
            .compact();
        u.setJwt_token(compactJws);
        repository.save(u);
        System.out.println(" OK : ");
        return Response.ok(" { \"status\": \"done\", \"jwt\": \"" + compactJws + "\" } ").build();
    }

    @Path("update-profile")
    @POST
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    @JWTTokenNeeded
    public Response updateProfile(
            String input
    ) {
        System.out.println(" input : " + input);
        Gson gson = new GsonBuilder().create();
        User p = gson.fromJson(input, User.class);
        System.out.println("p : " + p);
        EmailAddress emailaddy = new EmailAddress(p.getEmailAddress().getEmailAddress());
        User u = repository.findByEmailAddress(emailaddy);
        System.out.println(" u : " + u);
        if(u == null){
            return Response.status(403).build();
        }
        u.setFirstname(p.getFirstname());
        u.setLastname(p.getLastname());
        repository.save(u);
        return Response.ok(" { \"status\": \"done\" } ").build();
    }
}
